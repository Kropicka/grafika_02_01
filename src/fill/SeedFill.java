package fill;

import model.Point;
import rasterize.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SeedFill implements Filler {

    private Raster raster;
    private int backgroundColor = Color.black.getRGB();
    private int foregroundColor = Color.lightGray.getRGB();

    public SeedFill(Raster raster) {
        this.raster = raster;
    }

    public void fill(){
    fill(new Point(100,100));
    }

    @Override
    public void fill(Point p) {
        raster.setPixel(p.getX(),p.getY(),foregroundColor);

        if(raster.getPixel(p.getX()+1,p.getY()) == backgroundColor && p.getX()+1 < raster.getWidth()-1)
        {
            this.fill(new Point(p.getX()+1,p.getY()));
        }

        if(raster.getPixel(p.getX()-1,p.getY()) == backgroundColor && p.getX()-1 > 0)
        {
            this.fill(new Point(p.getX()-1,p.getY()) );
        }

        if(raster.getPixel(p.getX(),p.getY()+1) == backgroundColor && p.getY()+1 < raster.getHeight() -1)
        {
            this.fill(new Point(p.getX(),p.getY()+1));
        }

        if(raster.getPixel(p.getX(),p.getY()-1) == backgroundColor && p.getY()-1 > 0)
        {
            this.fill(new Point(p.getX(),p.getY()-1));
        }

        if(raster.getPixel(p.getX()-1,p.getY()) == Color.black.getRGB())
        {
            this.fill(new Point(p.getX()-1,p.getY()));
        }

        if(raster.getPixel(p.getX(),p.getY()+1) == Color.black.getRGB())
        {
            this.fill(new Point(p.getX(),p.getY()+1));
        }

        if(raster.getPixel(p.getX(),p.getY()-1) == Color.black.getRGB())
        {
            this.fill(new Point(p.getX(),p.getY()-1));
        }
    }

}
