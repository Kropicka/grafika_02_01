package render;

import model3D.Scene;
import model3D.Solid;
import rasterize.LineRasterizer;
import rasterize.*;
import transforms.Mat4;
import transforms.Mat4Identity;

public abstract class Renderer {
    protected Raster raster;
    protected LineRasterizer rasterizer;
    protected Mat4 view = new Mat4Identity();
    protected Mat4 projection = new Mat4Identity();
    public void render(Solid solid){};
    public void render(Scene scene){};
    public Renderer(Raster raster, LineRasterizer lineRasterizer){};

    public Mat4 getView() {
        return view;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public Mat4 getProjection() {
        return projection;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }
}
