package model;

import rasterize.DashedLineRasterizer;
import rasterize.FilledLineRasterizer;
import rasterize.Raster;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Polygon {

    //Deklarace
    private List<Point> points;
    private Raster raster;

    private FilledLineRasterizer lineCreator;
    private DashedLineRasterizer dashedLineCreator;
    private int currentNearest;

    //Konstruktor třídy
    public Polygon(Raster R) {
        points = new ArrayList<>();
        this.raster = R;

        lineCreator = new FilledLineRasterizer(raster);
        dashedLineCreator =  new DashedLineRasterizer(raster);
    }

    //rasterizace linii mezi uloženými body.
    public void rasterize() {
        if ( points.size()>= 2){
            for (int i = 1; i < points.size(); i++) { //Je potřeba projít celé pole s body
            lineCreator.rasterize(points.get(i-1).x,points.get(i-1).getY(),points.get(i).x,points.get(i).y,Color.yellow);
            }
            if (points.size()>=3){
                lineCreator.rasterize(points.get(0).x,points.get(0).getY(),points.get(points.size() -1).x,points.get(points.size() -1).y,Color.yellow);}
        }
    }

    //rasterizace po podržení tlačítka myši, která se ještě tvrdě nezapíše do listu bodů
    public void showWhere(Point p){
        if ( points.size()>= 1){
            for (int i = 1; i < points.size(); i++) {
                dashedLineCreator.rasterize(points.get(i-1).x,points.get(i-1).getY(),points.get(i).x,points.get(i).y,Color.red);
            }
            dashedLineCreator.rasterize(points.get(0).x,points.get(0).getY(),p.x,p.y,Color.red);
            dashedLineCreator.rasterize(p.x,p.y,points.get(points.size() -1).x,points.get(points.size() -1).y,Color.red);
        }


    }


    //Metoda pro přidání bodu
    public void addPoint(Point J){
        this.points.add(J);

        System.out.println("Počet bodů v zásobníku: " + points.size());
    }

    //Metoda pro "Restart" tlačítkem C
    public void clear(){
        points.clear();
        System.out.println("Zásobník polygonů vyčištěn");
    }

    public void removeLast() { //Odebrání posledního bodu
        if (points.size() > 1) {

            points.remove(points.size() - 1);
            System.out.println("Počet bodů v zásobníku: " + points.size());
        }else if (points.size() == 1){
            System.out.println("Poslední bod odebrán. Zásobník je prázdný");
            points.remove(points.size() - 1);

        }else{
            System.out.println("Nedostatek bodů k odebrání");
        }
    }

    public void findNearest(Point J){
        int dis = (int)Math.sqrt((points.get(0).x - J.getX()) * (points.get(0).x - J.getX()) + (points.get(0).y - J.getY()) * (points.get(0).y - J.getY()));
        currentNearest = 0;
        for (int i = 0; i < points.size(); i++) {
            if ((int)Math.sqrt((points.get(i).x - J.getX()) * (points.get(i).x - J.getX()) + (points.get(i).y - J.getY()) * (points.get(i).y - J.getY())) < dis){
                dis = (int)Math.sqrt((points.get(i).x - J.getX()) * (points.get(i).x - J.getX()) + (points.get(i).y - J.getY()) * (points.get(i).y - J.getY()));
                currentNearest = i;
            }
        }
        dashedLineCreator.rasterize(points.get(currentNearest).x,points.get(currentNearest).y,J.getX(),J.getY(),Color.RED);
    }

    public void showNearestReplacePoint(Point J){
        dashedLineCreator.rasterize(points.get(currentNearest).x,points.get(currentNearest).y,J.getX(),J.getY(),Color.RED);
    }

    public void replaceNearest(Point J){
        points.set(currentNearest,J);
        System.out.println("Úprava bodu kompletní");
    }

    public List<Point> getPoints() {
        return points;
    }
}
