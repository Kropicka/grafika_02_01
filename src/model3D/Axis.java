package model3D;

import transforms.Point3D;

public class Axis extends Solid {
    public Axis(){
        super();
        vertexBuffer.add(new Point3D(0, 0,0));

        vertexBuffer.add(new Point3D(2, 0,0));
        vertexBuffer.add(new Point3D(1.8, 0.1,0));
        vertexBuffer.add(new Point3D(1.8, -0.1,0));


        vertexBuffer.add(new Point3D(0, 2,0));
        vertexBuffer.add(new Point3D(0, 1.8,0.1));
        vertexBuffer.add(new Point3D(0, 1.8,-0.1));


        vertexBuffer.add(new Point3D(0, 0,2));
        vertexBuffer.add(new Point3D(0.1, 0,1.8));
        vertexBuffer.add(new Point3D(-0.1, 0,1.8));

    }
}
