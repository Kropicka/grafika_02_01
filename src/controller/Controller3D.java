package controller;

import fill.ScanLine;
import model3D.Cube;
import model3D.Solid;
import rasterize.FilledLineRasterizer;
import rasterize.Raster;
import render.WireframeEngine;
import transforms.*;
import view.Panel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Controller3D {

    private final Panel panel;
    private final Raster raster;

    private ScanLine lineFillMachine;

    private WireframeEngine wireframeMachine;

    private Camera camera;
    private Solid solid = new Cube();

    private int mouseLastX = 0;
    private int mouseLastY = 0;

    public Controller3D (Panel panel) {
     this.panel = panel;
     this.raster = panel.getRaster();
     wireframeMachine = new WireframeEngine(raster,new FilledLineRasterizer(raster));

    camera = new Camera()
                .withPosition(new Vec3D(15,0,0))
                .withAzimuth(Math.PI)
                .withZenith(0)
                .withFirstPerson(true);





    paintSolids();

    //Inicializační metoda
    initListeners();
    }

    private void initListeners() { //Inicializační metoda (Potřebná pro listenery
         int x1 = 0;
        panel.addMouseListener(new MouseAdapter() {
            //stlačené tlačítko
            @Override
            public void mousePressed(MouseEvent e) { //Odposlouchávání ztlačení myši
                int x1 = e.getX();
            }

            @Override
            public void mouseReleased(MouseEvent e) { //Přesun bodu

            }
        });

        panel.addMouseMotionListener(new MouseAdapter() {
            //Pohnutí myši

            @Override
            public void mousePressed(MouseEvent e) {
                mouseLastX = e.getX();
                mouseLastY = e.getY();

            }

            @Override
            public void mouseDragged(MouseEvent e) { //Odposlouchávání pro pohnutí myší (Znázornění vygenerování)
                System.out.println(mouseLastY );
                System.out.println(e.getY());
                System.out.println(e.getY() - mouseLastY);



                if ((e.getX() - mouseLastX) == -1) {
                    camera = camera.addAzimuth(Math.PI / 5000);
                }

                if ((e.getX() - mouseLastX) == 1) {
                    camera = camera.addAzimuth(-(Math.PI / 5000));
                }

                if ((e.getY() - mouseLastY) == -1) {
                    camera = camera.addZenith(Math.PI / 5000);
                }

                if ((e.getY() - mouseLastY) == 1) {
                    camera = camera.addZenith(-(Math.PI / 5000));
                }

                //camera = camera.addZenith((e.getY() - centerY) / 500000.0);
                clear();
                paintSolids();

                mouseLastX = e.getX();
                mouseLastY = e.getY();
            }


        });

        panel.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) { //Adapter pro odposlouchávání
                double step = 0.1;
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    camera =camera.left(step);
                }

                if (e.getKeyCode() == KeyEvent.VK_D) {
                    camera =camera.right(step);
                }

                if (e.getKeyCode() == KeyEvent.VK_W) {
                    camera =camera.up(step);
                }

                if (e.getKeyCode() == KeyEvent.VK_S) {
                    camera =camera.down(step);

                }

                clear();
                paintSolids();
            }


        });
    }

    public void clear() {
        raster.clear();
    }

    private void paintSolids(){
        Vec3D e = new Vec3D(10,0,0);
        Vec3D v = new Vec3D(-1,0,0);
        Vec3D u = new Vec3D(0,0,1);
        Mat4 view = new Mat4ViewRH(e,v,u);
        wireframeMachine.setView(view);
        wireframeMachine.setView(camera.getViewMatrix());
        wireframeMachine.setProjection(new Mat4OrthoRH(6,4,0.1,30));
        wireframeMachine.render(solid);

        wireframeMachine.renderAxis();
    }
}

